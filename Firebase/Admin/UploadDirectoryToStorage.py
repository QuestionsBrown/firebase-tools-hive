
import os
import firebase_admin
from firebase_admin import credentials
from firebase_admin import storage

'''
    For the given path, get the List of all files in the directory tree 
'''


def get_list_of_files(dir_name, appendix):
    # create a list of file and sub directories
    # names in the given directory
    list_of_file = os.listdir(dir_name)
    all_files = list()
    # Iterate over all the entries
    for entry in list_of_file:
        # Create full path
        full_path = os.path.join(dir_name, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(full_path):
            all_files = all_files + get_list_of_files(full_path, appendix + entry + "/")
        else:
            all_files.append((appendix + entry, full_path))

    return all_files


cred = credentials.Certificate("admin-sdk-key.json")
firebase_admin.initialize_app(cred, {
    'storageBucket': 'robinbird-firebasetools.appspot.com'
})

bucket = storage.bucket()
dirName = "C:\\Projects\\RobinBirdStudios\\Hives\\FirebaseToolsHive\\Unity\\Addressables\\ServerData\\StandaloneWindows64"

# Get the list of all files in directory tree at given path
listOfFiles = get_list_of_files(dirName, "assets/StandaloneWindows64/")

# Print the files
for elem in listOfFiles:
    print(elem)
    blob = bucket.blob(elem[0])
    blob.upload_from_filename(elem[1])