# Firebase Tools Hive

Example usage of Firebase Tools Library which can be found [HERE](https://gitlab.com/robinbird-studios/libraries/unity-plugins/firebase-tools)

Check the [.gitignore](https://gitlab.com/robinbird-studios/hives/firebase-tools-hive/blob/master/.gitignore) to see which files you have to add yourself to make it work.

Especially the `google-services.json` and `GoogleService-Info.plist` are important for Firebase.


Please open issues on the library project [HERE](https://gitlab.com/robinbird-studios/libraries/unity-plugins/firebase-tools)